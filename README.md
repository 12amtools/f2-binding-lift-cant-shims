# Customizable 3D Printed Lift/Cant shims for F2 Snowboard Bindings

Allows generating .STL file for customized shims for F2 snowboard bindings (F2 Race/Intec Titanium and Intec Titanflex). Shims may be customized for lift (degrees, heel/toe), cant (degrees, left/right), base depth (mm), and size (S, M, L).


## Getting Started

This .scad file can be utilized directly in OpenSCAD (http://www.openscad.org/), or via the Thingiverse Customizer (https://www.thingiverse.com/thing:2865271).

### Prerequisites

If using directly in OpenSCAD, OpenSCAD must be downloaded and installed on your machine. It using via the Thingiverse Customizer, no local download or installation is required. (Note that the Thingiverse Customzier can be very slow, depending on load at the time you attempt to use it, so some persistence and patience may be required...)

# DISCLAIMER

Snowboarding is an inherently dangerous activity, with significant risk of damage, injury or death, including due to equipment failure. Modification of equipment can greatly increase these risks. Use of this design is entirely at your own peril.

I accept no liability for any consequential loss, damage or expense of any kind arising from, or in consequence of, any fault or defect of any nature whatsoever in this design or its use. I can accept no liability for death, injury, loss or damage of any kind whatsoever whether to person or property however caused, arising out of or in connection with the use of this design. I accept no liability in respect of loss or damage to third parties caused directly or indirectly by use of this design.


## Authors

* **Jim Stoll** - *Initial work*

## License

This project is licensed under the Creative Commons - Attribution - Non-Commercial license.

## Acknowledgments

* Thanks to the members of the AlpineSnowboarder community (http://alpinesnowboarder.com) for inspiration, encouragement and helpful input and feedback provided during the development of this project.

## Version History

### [0.1.0] - 2018-04-19
#### Added
- Initial release version 


